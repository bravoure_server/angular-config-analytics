
(function () {
    'use strict';

    // Analytics Provider
    function configAnalytics ($analyticsProvider) {
        $analyticsProvider.virtualPageviews(true);
        $analyticsProvider.firstPageview(false);
        $analyticsProvider.withBase(true);

        if (data.$analyticsProviderRoutes != null) {
            $analyticsProvider.excludeRoutes(data.$analyticsProviderRoutes);
        }
    }

    configAnalytics.$inject =['$analyticsProvider'];

    angular
        .module('bravoureAngularApp')
        .config(configAnalytics);

})();
