# Bravoure - Config Analytics

## Use

It is used to see the base set up for angulartics


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-config-analytics": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
    
or in the root of the project execute:
    
    ./update
